# HelloWorld Adidas API 
## Simple CI/CD hello world API 

[![Codefresh build status]( https://g.codefresh.io/api/badges/pipeline/11nines/Adidas%20Demo%2Fhelloworld-adidas?type=cf-1&key=eyJhbGciOiJIUzI1NiJ9.NjA0MmJiMzIyNTk1YmJiYzExMmRmZDNi.9U-F1G3kTkFkP-3kPb5h9LaxZ19hUod-x79GtkU__2A)]( https://g.codefresh.io/pipelines/edit/new/builds?id=6042c2599896257c4dded199&pipeline=helloworld-adidas&projects=Adidas%20Demo&projectId=6042c16b9896257d15ded194)

## What is this repository for?
- CI/CD Demo. Deploy HelloWorld API on Kubernetes


## Features

- [Helm charts][helm]. Set up HPA, ingress, deploy, service and service accounts in charts/helloworld-adidas/values.yaml  
- [Sonarcloud][sonar] static code analysis in bitBucket pipeline
- [Trivy][trivy] Vulnerability Scanner for Containers and dependencies - package-lock.json in Codefresh
- [Codefresh][codefresh] and Jenkins pipelines as code.

## WorkFlow
- Work in a branch
- Every commit in any brach triggers bitBucket pipeline and the results are sent to SonarCloud
- Merge a pull request triggers Codefresh pipeline. If code and security tests, the API is deployed in kubernetes using a Helm chart.
- Deployments Rollbacks are perform with a new commit in master. Traceability. 

#### Jenkins pipeline is WIP. 
Jenkins server is set up for small deployments on kubernetes but the Jenkinsfile in this repository is a template WIP.


## License

MIT

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [trivy]: <https://github.com/aquasecurity/trivyr>
   [sonar]: <https://sonarcloud.io>
   [helm]: <https://helm.sh/>
   [codefresh]: <https://codefresh.io/>

