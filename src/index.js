const express = require('express');
const app = express();

var AWSXRay = require('aws-xray-sdk');
app.use(AWSXRay.express.openSegment('helloworld-adidas'));


app.get('/', (req, res) => {
  console.log('Hello world adidas received a request.');

  const target = process.env.TARGET || 'World';
  res.send(`Hello ${target} from Adidas!\n`);
});

app.use(AWSXRay.express.closeSegment());

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log('Hello world adidas listening on port', port);
});
